package lamrani.example.com.tagueri_training.Utils;

/**
 * Created by Lamrani on 19/09/2018.
 */

public class Constants {

    public static String SP_LOGIN = "SP_LOGIN";
    public static String SP_CONNECTED_USER = "SP_CONNECTED_USER";

    public static int TYPE_START = 0;
    public static int TYPE_END = 1;

    public static String TASK = "TASK";

    public static String USER = "USER";

    public static  String PRIORITY_HIGH = "high";
    public static String PRIORITY_MEDIUM = "medium";
    public static String PRIORITY_LOW = "low";



}
