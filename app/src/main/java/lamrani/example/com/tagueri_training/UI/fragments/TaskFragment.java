package lamrani.example.com.tagueri_training.UI.fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import lamrani.example.com.tagueri_training.Models.Task;
import lamrani.example.com.tagueri_training.R;
import lamrani.example.com.tagueri_training.UI.MainActivity;
import lamrani.example.com.tagueri_training.Utils.Constants;
import lamrani.example.com.tagueri_training.Utils.Helper;

/**
 * Created by Lamrani on 19/09/2018.
 */

public abstract class TaskFragment extends Fragment implements  DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    protected View view;
    protected EditText input_label, input_note;
    protected Button btn_save;
    protected TextView tv_start_date, tv_end_date, tv_start_time, tv_end_time;
    protected int date_type, time_type;
    protected RadioGroup radioGroup;
    protected Switch switch_status;
    protected MainActivity activity;


    @Override
    public void onResume() {
        super.onResume();
        activity.findViewById(R.id.searchView).setVisibility(View.GONE);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add_task, container, false);
        activity = (MainActivity) getActivity();
        return view;
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        configView();

        initializeValues();
    }

    /**
     * Initializing the values of the textviews and inputs
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    protected abstract void initializeValues();

    /**
     * Configure UI components
     */
    private void configView() {
        input_label = view.findViewById(R.id.input_task_label);
        input_note = view.findViewById(R.id.input_task_notes);
        btn_save = view.findViewById(R.id.btn_save_task);
        tv_start_date = view.findViewById(R.id.tv_start_date);
        tv_start_time = view.findViewById(R.id.tv_start_time);
        tv_end_date = view.findViewById(R.id.tv_end_day);
        tv_end_time = view.findViewById(R.id.tv_end_time);

        // radio for priority levels
        radioGroup = view.findViewById(R.id.radio_priority_level);

        // switch for status (in progress or done)
        switch_status = view.findViewById(R.id.switch_status);

        // btn save click listener
        btn_save.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {
                save();
            }
        });

        // start time listener
        tv_start_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                time_type = Constants.TYPE_START;
                showTimePickerDialog(view);
            }
        });

        // end time listener
        tv_end_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                time_type = Constants.TYPE_END;
                showTimePickerDialog(view);
            }
        });

        // start date listener
        tv_start_date.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {
                date_type = Constants.TYPE_START;
                showDayPickerDialog(view);
            }
        });

        // start date listener
        tv_end_date.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {
                date_type = Constants.TYPE_END;
                showDayPickerDialog(view);
            }
        });
    }

    /**
     * Show the time picker dialog
     * @param v the view
     */
    public void showTimePickerDialog(View v) {
        Calendar now = Calendar.getInstance();
        TimePickerDialog dpd = new TimePickerDialog(
                activity,
                this,
                8, // Initial year selection
                0, // Initial month selection
                true // Inital day selection
        );
        dpd.show();
    }

    /**
     * Show the day picker dialog
     * @param v the view
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void showDayPickerDialog(View v) {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = new DatePickerDialog(
                activity,
                this,
                now.get(Calendar.YEAR), // Initial year selection
                now.get(Calendar.MONTH), // Initial month selection
                now.get(Calendar.DAY_OF_MONTH) // Inital day selection
        );
        // if the start date is already set we set the min value of the end date to this value
        if(date_type == Constants.TYPE_END){
            String st_start_date = tv_start_date.getText().toString();
            Date start_date = Helper.getDate(st_start_date);
            if(start_date != null){
                dpd.getDatePicker().setMinDate(start_date.getTime());
            }
        }
        dpd.show();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
        // Do something with the date chosen by the user
        String st_date = convertDate(dayOfMonth) + "/" + convertDate((monthOfYear+1))  + "/" + year;

        if(date_type == Constants.TYPE_START){
            tv_start_date.setText(st_date);
        }
        else if(date_type == Constants.TYPE_END){
            tv_end_date.setText(st_date);
        }

    }

    @Override
    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
        // Do something with the time chosen by the user
        String time = convertDate(hourOfDay) + ":" + convertDate(minute);

        if(time_type == Constants.TYPE_START){
            tv_start_time.setText(time);
        }
        else if(time_type == Constants.TYPE_END){
            tv_end_time.setText(time);
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    public abstract void save();

    public String getSelectedPriorityLevel() {
        int selectedId = radioGroup.getCheckedRadioButtonId();
        // find the radiobutton by returned id
        RadioButton radioButton = view.findViewById(selectedId);
        String priority_level = radioButton.getText().toString();

        if(priority_level.equals(getString(R.string.high))){
            return Constants.PRIORITY_HIGH;
        }
        else if(priority_level.equals(getString(R.string.medium))){
            return Constants.PRIORITY_MEDIUM;
        }
        else if(priority_level.equals(getString(R.string.low))){
            return Constants.PRIORITY_LOW;
        }

        return null;
    }

    public String convertDate(int input) {
        if (input >= 10) {
            return String.valueOf(input);
        } else {
            return "0" + String.valueOf(input);
        }
    }
}
