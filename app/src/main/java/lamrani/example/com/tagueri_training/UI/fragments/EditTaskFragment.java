package lamrani.example.com.tagueri_training.UI.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import lamrani.example.com.tagueri_training.Database.TasksOperations;
import lamrani.example.com.tagueri_training.Models.Task;
import lamrani.example.com.tagueri_training.Models.User;
import lamrani.example.com.tagueri_training.R;
import lamrani.example.com.tagueri_training.UI.MainActivity;
import lamrani.example.com.tagueri_training.Utils.Constants;
import lamrani.example.com.tagueri_training.Utils.Helper;

/**
 * Created by Lamrani on 19/09/2018.
 */

public class EditTaskFragment extends TaskFragment {

    Button btn_delete;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_edit_task, container, false);
        activity = (MainActivity) getActivity();
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btn_delete = view.findViewById(R.id.btn_delete_task);
        btn_delete.setOnClickListener(new DeleteClickListener());

    }

    @Override
    public void onResume() {
        super.onResume();

        // set the title
        if(activity.getSupportActionBar() != null){
            activity.setTitle(getString(R.string.edit_task));
        }
    }

    /**
     * Get the task that has been sent in arguments from the previous fragment
     * @return
     */
    public Task getTask(){
        if(getArguments() != null){
            return (Task) getArguments().getSerializable(Constants.TASK);
        }
        return null;
    }

    @Override
    public void initializeValues() {
        // set the values
        Task task = getTask();
        // set the label
        input_label.setText(task.getLabel());
        // set the notes
        if(task.getNote() != null){
            input_note.setText(task.getNote());
        }
        // set the dates
        tv_start_date.setText(task.getStart_Day());
        tv_start_time.setText(task.getStart_time());
        tv_end_date.setText(task.getEnd_Day());
        tv_end_time.setText(task.getEnd_time());

        // set priority level
        String priority_level = task.getPriority_level();
        RadioButton radio = null;
        if(priority_level.equals(Constants.PRIORITY_HIGH)){
             radio = view.findViewById(R.id.radioHigh);
        }
        else if(priority_level.equals(Constants.PRIORITY_MEDIUM)){
             radio = view.findViewById(R.id.radioMedium);
        }
        else if(priority_level.equals(Constants.PRIORITY_LOW)){
             radio = view.findViewById(R.id.radioLow);
        }

        if(radio != null){
            radio.setChecked(true);
        }

        // set the status
        switch_status.setChecked(task.isIs_done());
    }

    /**
     * Saving the task
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void save() {
        // Getting the full start and end dates from string, then we are going to compare them (start date
        String st_start_date = tv_start_date.getText().toString() + " " + tv_start_time.getText().toString();
        String st_end_date = tv_end_date.getText().toString() + " " + tv_end_time.getText().toString();
        String label = input_label.getText().toString();
        String note = input_note.getText().toString();

        // get selected radio button from radioGroup for priority level
        int selectedId = radioGroup.getCheckedRadioButtonId();
        // find the radiobutton by returned id
        RadioButton radioButton = view.findViewById(selectedId);
        String priority_level = getSelectedPriorityLevel();

        Date start_date = Helper.getDateTime(st_start_date);
        Date end_date = Helper.getDateTime(st_end_date);

        // verifying that all the fields are not empty
        if(start_date != null && end_date != null && !label.isEmpty()){
            // check if the start date is less than the end date
            if(start_date.after(end_date)){
                Toast.makeText(activity, getString(R.string.verify_the_time), Toast.LENGTH_SHORT).show();
            }
            else{

                Task task = getTask();
                // set the label
                task.setLabel(label);
                // set the note
                task.setNote(note);
                // set the start date
                task.setStart_date(st_start_date);
                // set the end date
                task.setEnd_date(st_end_date);
                // set the priority level
                task.setPriority_level(priority_level);
                // set the task status
                task.setIs_done(switch_status.isChecked());

                // save it into database
                TasksOperations tasksOperations = new TasksOperations(activity);
                tasksOperations.open();
                tasksOperations.updateTask(task);
                tasksOperations.close();

                Toast.makeText(activity, getString(R.string.task_created_successfully), Toast.LENGTH_SHORT).show();
                activity.onBackPressed();
            }
        }
        else{
            Toast.makeText(activity, getString(R.string.verify_the_fields), Toast.LENGTH_SHORT).show();
        }
    }


    private class DeleteClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {



            final Runnable delete = new Runnable() {
                @Override
                public void run() {
                    TasksOperations tasksOperations = new TasksOperations(activity);
                    tasksOperations.open();
                    tasksOperations.removeTask(getTask().getId());
                    Toast.makeText(activity, getString(R.string.task_deleted), Toast.LENGTH_SHORT).show();
                    tasksOperations.close();
                    activity.onBackPressed();
                }
            };

            new AlertDialog.Builder(activity)
                    .setTitle(getString(R.string.app_name))
                    .setMessage(getString(R.string.confirm_delete))
                    .setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            delete.run();
                        }
                    })
                    .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
    }
}
