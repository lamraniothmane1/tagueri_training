package lamrani.example.com.tagueri_training.UI;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import lamrani.example.com.tagueri_training.Database.TasksOperations;
import lamrani.example.com.tagueri_training.Database.UsersOperations;
import lamrani.example.com.tagueri_training.Models.Task;
import lamrani.example.com.tagueri_training.Models.User;
import lamrani.example.com.tagueri_training.R;
import lamrani.example.com.tagueri_training.UI.fragments.AboutFragment;
import lamrani.example.com.tagueri_training.UI.fragments.AllUsersTasksFragment;
import lamrani.example.com.tagueri_training.UI.fragments.SettingsFragment;
import lamrani.example.com.tagueri_training.UI.fragments.UserTasksFragment;
import lamrani.example.com.tagueri_training.Utils.Constants;
import lamrani.example.com.tagueri_training.Utils.Helper;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    NavigationView navigationView;
    long id_user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // set the locale language
        Helper.setLocale(MainActivity.this);

        // show User tasks when the user just open the application and get logged in
        showFragment(new UserTasksFragment());

        // configure navigation view
        configureNavigationView();

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        refreshNavigationView();
    }

    public void refreshNavigationView() {
        navigationView.getMenu().clear();
        navigationView.inflateMenu(R.menu.activity_main_drawer);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Helper.setLocale(MainActivity.this);
        refreshNavigationView();
    }

    /**
     * Navigation view configuration
     */
    private void configureNavigationView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        // set the listener on each item
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // set the name and the email of the current connected user
        setUserInformations();
    }

    /**
     * Set user informations
     */
    private void setUserInformations() {
        View headerView = navigationView.getHeaderView(0);
        TextView tv_name = headerView.findViewById(R.id.tv_name);
        TextView tv_email = headerView.findViewById(R.id.tv_email);

        // get the User
        User user = Helper.getCurrentUser(getApplicationContext());

        tv_email.setText(user.getEmail());
        tv_name.setText(user.getFull_name());
    }

    @Override
    protected void onResume() {
        super.onResume();
        // set the locale and refresh the navigation drawer
        Helper.setLocale(getApplicationContext());
        refreshNavigationView();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }

        // If there is no fragment to show then we finish the current activity
        FragmentManager fragmentManager = getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        if(fragments.isEmpty()){
            finish();
        }
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/

    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        hiddeKeyboard();

        switch (id){
            case R.id.nav_home: {
                showFragment(new UserTasksFragment());
                break;
            }
            case R.id.nav_all_tasks: {
                showFragment(new AllUsersTasksFragment());
                break;
            }
            case R.id.nav_settings: {
                showFragment(new SettingsFragment());
                break;
            }
            case R.id.nav_about: {
                showFragment(new AboutFragment());
                break;
            }

            case R.id.nav_logout: {
                logout();
                break;
            }
            default: hideFragments();

        }

        // Close the navigation drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Logout method
     */
    private void logout() {

        final Runnable delete = new Runnable() {
            @Override
            public void run() {
                // delete the user_id from the shared preferences
                SharedPreferences prefs = getSharedPreferences(Constants.SP_LOGIN, Context.MODE_PRIVATE);
                if(prefs != null){
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putLong(Constants.SP_CONNECTED_USER, -1);
                    editor.apply();
                }

                // start Login activity
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        };

        // alert message
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.app_name))
                .setMessage(getString(R.string.confirm_logout))
                .setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        delete.run();
                    }
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    /**
     * Hide keyboard
     */
    public void hiddeKeyboard() {
        View focus = getCurrentFocus();
        if(focus != null){
            InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if(keyboard != null){
                keyboard.hideSoftInputFromWindow(focus.getWindowToken(), 0);
            }
        }
    }

    /**
     * Show fragment
     */
    public void showFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_frame, fragment, fragment.getClass().getName());
        fragmentTransaction.addToBackStack(fragment.getTag());
        fragmentTransaction.commit();
    }

    /**
     * Hide all fragments
     */
    public void hideFragments(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        for(Fragment fragment : fragments){
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.remove(fragment);
            fragmentTransaction.commit();
        }

    }



}
