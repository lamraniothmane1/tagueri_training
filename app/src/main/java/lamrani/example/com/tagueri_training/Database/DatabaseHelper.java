package lamrani.example.com.tagueri_training.Database;

/**
 * Created by Lamrani on 22/02/2018.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "tagueri_db";
    public static final int DB_VERSION = 6 ;

    public static final String CREATED_AT = "created_at";

    //Constants for Database name, table name, and column names

    // Tasks
    public static final String TASK_TABLE_NAME = "task";
    public static final String TASK_COLUMN_ID = "id";
    public static final String TASK_COLUMN_LABEL = "label";
    public static final String TASK_COLUMN_NOTE = "note";
    public static final String TASK_COLUMN_START_DATE = "start_date";
    public static final String TASK_COLUMN_END_DATE = "end_date";
    public static final String TASK_COLUMN_PRIORITY_LEVEL = "priority_level";
    public static final String TASK_COLUMN_IS_DONE = "is_done";
    public static final String TASK_COLUMN_USER_ID = "user_id";


    String SQL_CREATE_TASK_TABLE = "CREATE TABLE " + TASK_TABLE_NAME
            + "(" + TASK_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + TASK_COLUMN_LABEL + " VARCHAR NOT NULL, "
            + TASK_COLUMN_NOTE + " VARCHAR, "
            + TASK_COLUMN_START_DATE + " VARCHAR, "
            + TASK_COLUMN_END_DATE + " VARCHAR, "
            + TASK_COLUMN_PRIORITY_LEVEL + " VARCHAR, "
            + TASK_COLUMN_IS_DONE + " INTEGER, "
            + TASK_COLUMN_USER_ID + " INTEGER, "
            + CREATED_AT + " DATETIME DEFAULT CURRENT_TIMESTAMP );";

    // Users
    public static final String USER_TABLE_NAME = "user";
    public static final String USER_COLUMN_ID = "id";
    public static final String USER_COLUMN_FULL_NAME = "full_name";
    public static final String USER_COLUMN_EMAIL = "email";
    public static final String USER_COLUMN_PASSWORD = "password";


    String SQL_CREATE_USER_TABLE = "CREATE TABLE " + USER_TABLE_NAME
            + "(" + USER_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + USER_COLUMN_FULL_NAME + " VARCHAR NOT NULL, "
            + USER_COLUMN_EMAIL + " VARCHAR, "
            + USER_COLUMN_PASSWORD + " VARCHAR, "
            + CREATED_AT + " DATETIME DEFAULT CURRENT_TIMESTAMP );";


    //Constructor
    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    //creating the database
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TASK_TABLE);
        db.execSQL(SQL_CREATE_USER_TABLE);
    }

    //upgrading the database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + USER_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TASK_TABLE_NAME);

        onCreate(db);
    }

}
