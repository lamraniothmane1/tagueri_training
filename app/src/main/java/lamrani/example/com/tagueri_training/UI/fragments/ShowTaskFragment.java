package lamrani.example.com.tagueri_training.UI.fragments;

import android.app.Fragment;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.Toast;

import java.util.Date;

import lamrani.example.com.tagueri_training.Database.TasksOperations;
import lamrani.example.com.tagueri_training.Models.Task;
import lamrani.example.com.tagueri_training.Models.User;
import lamrani.example.com.tagueri_training.R;
import lamrani.example.com.tagueri_training.Utils.Helper;

/**
 * Created by Lamrani on 19/09/2018.
 */

public class ShowTaskFragment extends EditTaskFragment {

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // hide delete and save button
        btn_save.setVisibility(View.GONE);
        btn_delete.setVisibility(View.GONE);

        input_label.setEnabled(false);
        input_note.setEnabled(false);
        tv_start_date.setClickable(false);
        tv_start_time.setClickable(false);
        tv_end_date.setClickable(false);
        tv_end_time.setClickable(false);
        radioGroup.setEnabled(false);
        view.findViewById(R.id.radioLow).setClickable(false);
        view.findViewById(R.id.radioMedium).setClickable(false);
        view.findViewById(R.id.radioHigh).setClickable(false);
        switch_status.setClickable(false);

    }


    @Override
    public void onResume() {
        super.onResume();
        activity.setTitle(getString(R.string.details));
    }
}
