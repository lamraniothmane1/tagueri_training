package lamrani.example.com.tagueri_training.UI;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import lamrani.example.com.tagueri_training.R;
import lamrani.example.com.tagueri_training.Utils.Constants;
import lamrani.example.com.tagueri_training.Utils.Helper;

public class SplashActivity extends AppCompatActivity {

    public ImageView iv_logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // full screen activity
        if(getSupportActionBar() != null){
            getSupportActionBar().hide();
        }
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // get the logo imageview
        iv_logo = findViewById(R.id.iv_logo);

        // animate the image view (fading out)
        animate();

        // start the next activity
        start_next_activity();
    }

    /**
     * Start the new activity
     */
    private void start_next_activity() {
        // this thread will sleep 2 seconds before executing
        Thread timer = new Thread(){
            public void run(){
                try{
                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                finally {
                    // check if the user is already connected
                    Intent intent = null;
                    if (Helper.getCurrentUser(getApplicationContext()) != null){
                        intent = new Intent(SplashActivity.this, MainActivity.class);
                    }
                    else {
                        intent = new Intent(SplashActivity.this, LoginActivity.class);
                    }

                    startActivity(intent);
                    finish();
                }
            }
        };

        timer.start();
    }

    /**
     * Animate the logo to fade in when open activity
     */
    private void animate() {
        Animation myAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.splash_transition);
        if(iv_logo != null){
            iv_logo.startAnimation(myAnimation);
        }
    }
}
