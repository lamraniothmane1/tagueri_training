package lamrani.example.com.tagueri_training.UI.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.HorizontalScrollView;
import android.widget.SearchView;
import android.widget.TextView;

import com.whiteelephant.monthpicker.MonthPickerDialog;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import lamrani.example.com.tagueri_training.Adapters.AllUsersTasksAdapter;
import lamrani.example.com.tagueri_training.Database.TasksOperations;
import lamrani.example.com.tagueri_training.Database.UsersOperations;
import lamrani.example.com.tagueri_training.Models.Task;
import lamrani.example.com.tagueri_training.Models.User;
import lamrani.example.com.tagueri_training.R;
import lamrani.example.com.tagueri_training.UI.MainActivity;
import lamrani.example.com.tagueri_training.Utils.Helper;

/**
 * Created by Lamrani on 19/09/2018.
 */

public abstract class MainTasksFragment extends Fragment{

    protected View view;
    RecyclerView recyclerView;
    //AllUsersTasksAdapter allUsersTasksAdapter;
    private TextView btn_day, btn_week, btn_month, selected_btn, btn_all, tv_selection;
    protected Activity activity;
    private SearchView searchView;
    protected List<Task> user_tasks;
    private HorizontalScrollView filter_selection;
    private boolean is_menu_expanded;

    @Override
    public void onResume() {
        super.onResume();
        activity.setTitle(getString(R.string.menu_users_tasks));
        activity.findViewById(R.id.searchView).setVisibility(View.VISIBLE);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_all_tasks, container, false);
        activity = getActivity();
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        user_tasks = new ArrayList<>();
        is_menu_expanded = false;

        configView();

        initializeValues();



    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void initializeValues() {
        // set the current date to textViews
        String st_date = Helper.getDateString(new Date());
        tv_selection.setText(getString(R.string.today));

        Date date = Helper.getDate(st_date);
        refreshListView(0, date);

        select(btn_day);
    }

    /**
     * Initialize UI components
     */
    private void configView() {

        // btn for selection
        tv_selection = view.findViewById(R.id.tv_selection);
        btn_day =  view.findViewById(R.id.btn_day);
        btn_day.setOnClickListener(new SelectDayClickListener());
        btn_all = view.findViewById(R.id.btn_all);
        btn_all.setOnClickListener(new SelectAllClickListener());
        btn_week = view.findViewById(R.id.btn_week);
        btn_week.setOnClickListener(new SelectWeekClickListener());
        btn_month = view.findViewById(R.id.btn_month);
        btn_month.setOnClickListener(new SelectMonthClickListener());
        selected_btn = btn_day;
        select(btn_day);

        // filter selection
        filter_selection = view.findViewById(R.id.filter_selection);
        tv_selection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(is_menu_expanded){
                    hideFilter();
                }
                else{
                    showFilter();
                }
            }
        });

        // searchview
        searchView = activity.findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public boolean onQueryTextSubmit(String s) {
                if(s.equals("")){
                    select(btn_day);
                    initializeValues();
                }
                else{
                    filterList(s);
                }
                return false;
            }

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public boolean onQueryTextChange(String s) {
                if(s.equals("")){
                    select(btn_day);
                    initializeValues();
                }
                else{
                    filterList(s);
                    tv_selection.setText(getString(R.string.search));
                }

                return false;
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public boolean onClose() {
                select(btn_day);
                initializeValues();
                return false;
            }
        });

        configureRecyclerView();
    }

    private void hideFilter() {
        filter_selection.setVisibility(View.GONE);
        is_menu_expanded = false;
    }

    private void showFilter() {
        filter_selection.setVisibility(View.VISIBLE);
        is_menu_expanded = true;
    }


    // abstract methods
    protected abstract void removeAllItems();
    protected abstract void addItem(Task task);
    protected abstract void configureRecyclerView();
    protected abstract void filterList(String s);
    protected abstract void refreshListView(int code, Date date);


    private void select(TextView btn) {
        selected_btn.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(activity, R.color.white)));
        selected_btn.setTextColor(ContextCompat.getColor(activity, R.color.selected));
        selected_btn.setTypeface(Typeface.DEFAULT);
        selected_btn = btn;
        btn.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(activity, R.color.selected)));
        selected_btn.setTextColor(ContextCompat.getColor(activity, R.color.white));
        selected_btn.setTypeface(btn.getTypeface(), Typeface.BOLD);

    }

    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month-1];
    }

    protected void populateRecyclerView(List<Task> user_tasks) {
        // remove all the items
        removeAllItems();

        if(user_tasks.isEmpty()){
            view.findViewById(R.id.empty_list).setVisibility(View.VISIBLE);
        }
        else{
            for (Task task : user_tasks) {
                view.findViewById(R.id.empty_list).setVisibility(View.GONE);
                addItem(task);
            }
        }

    }


    private class SelectDayClickListener implements View.OnClickListener, DatePickerDialog.OnDateSetListener {
            @Override
            public void onClick(View view) {
                // show a date Picker dialog
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = new DatePickerDialog(
                        activity,
                        this,
                        now.get(Calendar.YEAR), // Initial year selection
                        now.get(Calendar.MONTH), // Initial month selection
                        now.get(Calendar.DAY_OF_MONTH) // Inital day selection
                );
                // if the start date is already set we set the min valu
                dpd.show();


            }

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                // select the view
                select(btn_day);
                // Do something with the date chosen by the user
                String st_date = dayOfMonth + "/" + (monthOfYear+1)  + "/" + year;
                tv_selection.setText(st_date);

                Date date = Helper.getDate(st_date);
                refreshListView(0, date);

                // hide the filter
                hideFilter();
            }
        }

        private class SelectWeekClickListener implements View.OnClickListener, DatePickerDialog.OnDateSetListener {
            @Override
            public void onClick(View view) {

                // show a date Picker dialog
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = new DatePickerDialog(
                        activity,
                        this,
                        now.get(Calendar.YEAR), // Initial year selection
                        now.get(Calendar.MONTH), // Initial month selection
                        now.get(Calendar.DAY_OF_MONTH) // Inital day selection
                );
                // if the start date is already set we set the min valu
                dpd.show();


            }

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                // select the view
                select(btn_week);

                // Do something with the date chosen by the user
                String st_date = dayOfMonth + "/" + (monthOfYear+1)  + "/" + year;
                String st_end_date = "--";

                Date start_date = Helper.getDate(st_date);

                // get Calendar instance
                Calendar cal = Calendar.getInstance();
                cal.setTime(start_date);
                // add 7 days
                cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) + 7);

                // convert to date
                Date end_date = cal.getTime();
                st_end_date = Helper.getDateString(end_date);

                tv_selection.setText(st_date + " - " + st_end_date);

                Date date = Helper.getDate(st_date);
                refreshListView(1, date);

                // hide the filter
                hideFilter();
            }
        }

        private class SelectMonthClickListener implements View.OnClickListener, MonthPickerDialog.OnDateSetListener
        {
            @Override
            public void onClick(View view) {

                Calendar now = Calendar.getInstance();
                MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(activity,
                        this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH));
                builder.build().show();
            }

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onDateSet(int selectedMonth, int selectedYear) {
                // select the view
                select(btn_month);

                // Do something with the date chosen by the user
                String st_date = (selectedMonth+1) + "/" + selectedYear;
                tv_selection.setText(getMonth((selectedMonth+1))  + "/" + selectedYear);

                Date date = Helper.getDateFromMonth(st_date);
                refreshListView(2, date);

                // hide the filter
                hideFilter();
            }
        }


    /**
     * Select all click listener
     */
    private class SelectAllClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            select(btn_all);

            // update textview for selection
            tv_selection.setText(getString(R.string.all));

            refreshListView(3, null);
        }
    }
}
