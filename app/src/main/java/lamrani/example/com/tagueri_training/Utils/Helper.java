package lamrani.example.com.tagueri_training.Utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.util.DisplayMetrics;

import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

import lamrani.example.com.tagueri_training.Database.UsersOperations;
import lamrani.example.com.tagueri_training.Models.User;
import lamrani.example.com.tagueri_training.R;
import lamrani.example.com.tagueri_training.UI.LoginActivity;
import lamrani.example.com.tagueri_training.UI.MainActivity;
import lamrani.example.com.tagueri_training.UI.fragments.SettingsFragment;

/**
 * Created by Lamrani on 20/09/2018.
 */

public class Helper {

    public static long ID_USER = -1;

    /**
     * Get date from string
     * @param st_date
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    public static Date getDate(String st_date){
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            return formatter.parse(st_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static String getDateString(Date date){
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        return formatter.format(date);
    }


    /**
     * Get dateTime from string
     * @param st_date
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    public static Date getDateFromMonth(String st_date){
        SimpleDateFormat formatter = new SimpleDateFormat("MM/yyyy");
        try {
            return formatter.parse(st_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Get dateTime from string
     * @param st_date
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    public static Date getDateTime(String st_date){
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        try {
            return formatter.parse(st_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static User getCurrentUser(Context context){
        User user = null;
        SharedPreferences prefs = context.getSharedPreferences(Constants.SP_LOGIN, Context.MODE_PRIVATE);

        long connected_user = prefs.getLong(Constants.SP_CONNECTED_USER, -1);

        // check if the user exists in the database
        UsersOperations usersOperations = new UsersOperations(context);
        usersOperations.open();
        user = usersOperations.findUserByID(connected_user);

        if(user == null){
            user = usersOperations.findUserByID(Helper.ID_USER);
        }

        return user;
    }

    /**
     * Set current language
     */
    // Change locale application language
    public static void changeLocaleLanguage(Activity activity, String lang){
        final ProgressDialog progressDialog = new ProgressDialog(activity);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(activity.getString(R.string.loading));
        progressDialog.show();

        Locale myLocale = new Locale(lang);
        Resources res = activity.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);

        progressDialog.dismiss();

        //activity.showFragment(new SettingsFragment());

    }

    /**
     * set local langauage
     */
    public static void setLocale(Context context){
        // Shared preference
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String language_value = sharedPreferences.getString("language", "en");
        Locale locale = new Locale(language_value);
        Locale.setDefault(locale);
        Configuration config = context.getResources().getConfiguration();
        config.locale= locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
    }


}
