package lamrani.example.com.tagueri_training.Models;

import java.io.Serializable;

/**
 * Created by Lamrani on 19/09/2018.
 */

public class User implements Serializable{

    private long id;
    private String full_name;
    private String email;
    private String password;

    public User() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
