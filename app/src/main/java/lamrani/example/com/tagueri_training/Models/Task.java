package lamrani.example.com.tagueri_training.Models;

import java.io.Serializable;

/**
 * Created by Lamrani on 19/09/2018.
 */

public class Task implements Serializable{

    private long id;
    private String label;
    private String start_date;
    private String end_date;
    private String note;
    private long user_id;
    private boolean is_done;
    private String priority_level;

    public Task() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getStart_Day(){
        return start_date.split(" ")[0];
    }

    public String getStart_time(){
        return start_date.split(" ")[1];
    }

    public String getEnd_Day(){
        return end_date.split(" ")[0];
    }

    public String getEnd_time(){
        return end_date.split(" ")[1];
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public boolean isIs_done() {
        return is_done;
    }

    public void setIs_done(boolean is_done) {
        this.is_done = is_done;
    }

    public String getPriority_level() {
        return priority_level;
    }

    public void setPriority_level(String priority_level) {
        this.priority_level = priority_level;
    }
}
