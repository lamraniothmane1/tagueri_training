package lamrani.example.com.tagueri_training.UI;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.Locale;

import lamrani.example.com.tagueri_training.Database.UsersOperations;
import lamrani.example.com.tagueri_training.Models.User;
import lamrani.example.com.tagueri_training.R;
import lamrani.example.com.tagueri_training.Utils.Constants;
import lamrani.example.com.tagueri_training.Utils.Helper;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    EditText _email_text;
    EditText _password_text;
    Button _login_button;
    TextView _signup_link;
    CheckBox _check_remember_me;
    TextView tv_language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if(getSupportActionBar() != null){
            getSupportActionBar().hide();
        }

        // set the locale language
        Helper.setLocale(LoginActivity.this);

        // initialize UI components
        initView();

    }

    @Override
    protected void onResume() {
        super.onResume();

        Helper.setLocale(LoginActivity.this);
    }

    private void initView() {
        _email_text = findViewById(R.id.input_email);
        _password_text = findViewById(R.id.input_password);
        _signup_link = findViewById(R.id.tv_signup_link);
        _login_button = findViewById(R.id.btn_login);
        _check_remember_me = findViewById(R.id.check_remember_me);

        _login_button.setOnClickListener(new LoginClickListener());

        _signup_link.setOnClickListener(new RegisterClickListener());

        tv_language = findViewById(R.id.tv_language);
        tv_language.setOnClickListener(new ChangeLocaleClickListener());
        tv_language.setVisibility(View.GONE);

    }

    private class LoginClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            // check for validations
            login();
        }
    }

    @Override
    public void onBackPressed() {
        // disable going back to the MainActivity
        moveTaskToBack(true);
    }

    public void onLoginSuccess() {
        _login_button.setEnabled(true);

        // check if the user exists in the database
        UsersOperations usersOperations = new UsersOperations(getApplicationContext());
        usersOperations.open();
        User user = usersOperations.findUser(_email_text.getText().toString(), _password_text.getText().toString());
        if(user != null){
            // if the remember me check box is checked, then we are going to save this as a shared preference
            if(_check_remember_me.isChecked()){
                SharedPreferences prefs = getSharedPreferences(Constants.SP_LOGIN, Context.MODE_PRIVATE);
                if(prefs != null){
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putLong(Constants.SP_CONNECTED_USER, user.getId());
                    editor.apply();
                }
            }

            Helper.ID_USER = user.getId();

            // Connect the user to the Home activity
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            intent.putExtra(Constants.USER, user.getId());
            startActivity(intent);
            finish();

            Toast.makeText(getBaseContext(), "Login success", Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(this, getString(R.string.bad_credentials), Toast.LENGTH_SHORT).show();
        }



    }


    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();

        _login_button.setEnabled(true);
    }


    /**
     * Login
     */
    public void login() {
        Log.d(TAG, "Login");

        if (!validate()) {
            onLoginFailed();
            return;
        }

        _login_button.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.connection));
        progressDialog.show();

        // TODO: Implement your own authentication logic here.
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed
                        onLoginSuccess();
                        // onLoginFailed();
                        progressDialog.dismiss();
                    }
                }, 1000);
    }


    /**
     * Validating the email
     * @return
     */
    public boolean validate() {
        boolean valid = true;

        String email = _email_text.getText().toString();
        String password = _password_text.getText().toString();

        // checking the email
        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _email_text.setError(getString(R.string.mail_validation));
            valid = false;
        } else {
            _email_text.setError(null);

            // checking the password
            if (password.isEmpty() || password.length() < 4 ) {
                _password_text.setError(getString(R.string.password_validation));
                valid = false;
            } else {
                _password_text.setError(null);
            }
        }



        return valid;
    }

    private class RegisterClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
            startActivity(intent);
        }
    }

    private class ChangeLocaleClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {



        }
    }
}
