package lamrani.example.com.tagueri_training.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import lamrani.example.com.tagueri_training.Models.Task;
import lamrani.example.com.tagueri_training.Models.User;
import lamrani.example.com.tagueri_training.Utils.Helper;

/**
 * Created by Lamrani on 27/02/2018.
 */

public class TasksOperations {

    public static final String LOGTAG = "TASKS_OPERATIONS";
    SQLiteOpenHelper dbHandler;
    SQLiteDatabase database;
    private static final String[] allColumns = {
            DatabaseHelper.TASK_COLUMN_ID,
            DatabaseHelper.TASK_COLUMN_LABEL,
            DatabaseHelper.TASK_COLUMN_NOTE,
            DatabaseHelper.TASK_COLUMN_START_DATE,
            DatabaseHelper.TASK_COLUMN_END_DATE,
            DatabaseHelper.TASK_COLUMN_USER_ID,
            DatabaseHelper.TASK_COLUMN_PRIORITY_LEVEL,
            DatabaseHelper.TASK_COLUMN_IS_DONE
    };

    public TasksOperations(Context context){
        dbHandler = new DatabaseHelper(context);
    }

    public void open(){
        Log.i(LOGTAG,"Database Opened");
        database = dbHandler.getWritableDatabase();

    }

    public void close(){
        Log.i(LOGTAG, "Database Closed");
        dbHandler.close();
    }


    /**
     * Add new task
     * @param task
     * @return
     */
    public Task addTask(Task task){
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.TASK_COLUMN_LABEL, task.getLabel());
        values.put(DatabaseHelper.TASK_COLUMN_NOTE, task.getNote());
        values.put(DatabaseHelper.TASK_COLUMN_START_DATE, task.getStart_date());
        values.put(DatabaseHelper.TASK_COLUMN_END_DATE, task.getEnd_date());
        values.put(DatabaseHelper.TASK_COLUMN_USER_ID, task.getUser_id());
        values.put(DatabaseHelper.TASK_COLUMN_PRIORITY_LEVEL, task.getPriority_level());
        values.put(DatabaseHelper.TASK_COLUMN_IS_DONE, task.isIs_done());
        long insertId = database.insert(DatabaseHelper.TASK_TABLE_NAME,null,values);
        task.setId(insertId);
        return task;
    }

    /**
     * Get the list of all the tasks that match the user id
     * @return
     */
    public List<Task> getAllUserTasks(long user_id) {

        Cursor cursor = database.query(DatabaseHelper.TASK_TABLE_NAME, allColumns, DatabaseHelper.TASK_COLUMN_USER_ID + "=?",new String[]{String.valueOf(user_id)},null, null, DatabaseHelper.TASK_COLUMN_START_DATE);

        List<Task> tasks = new ArrayList<>();
        if(cursor.getCount() > 0){
            while(cursor.moveToNext()){
                Task task = new Task();
                task.setId(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_ID)));
                task.setLabel(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_LABEL)));
                task.setNote(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_NOTE)));
                task.setStart_date(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_START_DATE)));
                task.setEnd_date(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_END_DATE)));
                task.setUser_id(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_USER_ID)));
                task.setPriority_level(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_PRIORITY_LEVEL)));
                task.setIs_done(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_IS_DONE)) == 1 );
                tasks.add(task);
            }
        }
        // return All tasks
        return tasks;
    }

    /**
     * Get the list of all the tasks that match the user id
     * @return
     */
    public List<Task> getAllTasks() {

        Cursor cursor = database.query(DatabaseHelper.TASK_TABLE_NAME, allColumns, null, null,null, null, DatabaseHelper.TASK_COLUMN_START_DATE + " DESC");

        List<Task> tasks = new ArrayList<>();
        if(cursor.getCount() > 0){
            while(cursor.moveToNext()){
                Task task = new Task();
                task.setId(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_ID)));
                task.setLabel(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_LABEL)));
                task.setNote(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_NOTE)));
                task.setStart_date(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_START_DATE)));
                task.setEnd_date(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_END_DATE)));
                task.setUser_id(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_USER_ID)));
                task.setPriority_level(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_PRIORITY_LEVEL)));
                task.setIs_done(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_IS_DONE)) == 1 );
                tasks.add(task);
            }
        }
        // return All tasks
        return tasks;
    }

    public int getMonth(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        // check the month and the year
        return cal.get(Calendar.MONTH);
    }

    public int getYear(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        // check the month and the year
        return cal.get(Calendar.YEAR);
    }

    /**
     * Get tasks by month
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    public List<Task> getTasksByMonth(long id_user, Date date) {

        Cursor cursor = database.query(DatabaseHelper.TASK_TABLE_NAME, allColumns, null, null,null, null, DatabaseHelper.TASK_COLUMN_START_DATE + " DESC");
        if(id_user != -1){
            cursor = database.query(DatabaseHelper.TASK_TABLE_NAME, allColumns, DatabaseHelper.TASK_COLUMN_USER_ID + "=?",new String[]{String.valueOf(id_user)},null, null, DatabaseHelper.TASK_COLUMN_START_DATE + " DESC");
        }

        List<Task> tasks = new ArrayList<>();
        if(cursor.getCount() > 0){
            while(cursor.moveToNext()){

                // Check if the month match the month of the given
                String st_task_date = cursor.getString(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_START_DATE));
                Date task_date = Helper.getDate(st_task_date);

                // get the year and the month of the dates
                int task_month = getMonth(task_date);
                int month = getMonth(date);
                int task_year = getYear(task_date);
                int year = getYear(date);


                // comparaison under this condition ==> task_year ==  <= taskdate <= end_date
                if(year == task_year && month == task_month){
                    Task task = new Task();
                    task.setId(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_ID)));
                    task.setLabel(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_LABEL)));
                    task.setNote(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_NOTE)));
                    task.setStart_date(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_START_DATE)));
                    task.setEnd_date(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_END_DATE)));
                    task.setUser_id(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_USER_ID)));
                    task.setPriority_level(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_PRIORITY_LEVEL)));
                    task.setIs_done(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_IS_DONE)) == 1 );
                    tasks.add(task);
                }
            }
        }
        // return All tasks
        return tasks;
    }

    /**
     * Get tasks by date
     * @param date
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    public List<Task> getTasksByDate(long id_user, Date date) {

        Cursor cursor = database.query(DatabaseHelper.TASK_TABLE_NAME, allColumns, null, null,null, null, DatabaseHelper.TASK_COLUMN_START_DATE + " DESC");
        if(id_user != -1){
            cursor = database.query(DatabaseHelper.TASK_TABLE_NAME, allColumns, DatabaseHelper.TASK_COLUMN_USER_ID + "=?",new String[]{String.valueOf(id_user)},null, null, DatabaseHelper.TASK_COLUMN_START_DATE + " DESC");
        }

        List<Task> tasks = new ArrayList<>();
        if(cursor.getCount() > 0){
            while(cursor.moveToNext()){

                // Check if the dates are on the same day
                String st_task_date = cursor.getString(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_START_DATE));
                Date task_date = Helper.getDate(st_task_date);
                // comparaison
                if(task_date.compareTo(date) == 0){
                    Task task = new Task();
                    task.setId(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_ID)));
                    task.setLabel(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_LABEL)));
                    task.setNote(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_NOTE)));
                    task.setStart_date(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_START_DATE)));
                    task.setEnd_date(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_END_DATE)));
                    task.setUser_id(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_USER_ID)));
                    task.setPriority_level(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_PRIORITY_LEVEL)));
                    task.setIs_done(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_IS_DONE)) == 1 );
                    tasks.add(task);
                }
            }
        }
        // return All tasks
        return tasks;
    }

    /**
     * Get tasks by Week
     * @param start_date
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    public List<Task> getTasksByWeek(long id_user, Date start_date) {

        Cursor cursor = database.query(DatabaseHelper.TASK_TABLE_NAME, allColumns, null, null,null, null, DatabaseHelper.TASK_COLUMN_START_DATE + " DESC");
        if(id_user != -1){
            cursor = database.query(DatabaseHelper.TASK_TABLE_NAME, allColumns, DatabaseHelper.TASK_COLUMN_USER_ID + "=?",new String[]{String.valueOf(id_user)},null, null, DatabaseHelper.TASK_COLUMN_START_DATE + " DESC");
        }

        List<Task> tasks = new ArrayList<>();
        if(cursor.getCount() > 0){
            while(cursor.moveToNext()){

                // Check if the dates are on between the day given and day + 7
                String st_task_date = cursor.getString(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_START_DATE));
                Date task_date = Helper.getDate(st_task_date);

                // get Calendar instance to get the 7th day after that date
                Calendar cal = Calendar.getInstance();
                cal.setTime(start_date);
                // add 7 days
                cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) + 7);
                // convert to date
                Date end_date = cal.getTime();

                // comparaison under this condition ==> start_date <= taskdate <= end_date
                if((task_date.compareTo(start_date) == 0 ||  task_date.compareTo(end_date) == 0) || task_date.after(start_date) && task_date.before(end_date)){
                    Task task = new Task();
                    task.setId(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_ID)));
                    task.setLabel(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_LABEL)));
                    task.setNote(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_NOTE)));
                    task.setStart_date(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_START_DATE)));
                    task.setEnd_date(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_END_DATE)));
                    task.setUser_id(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_USER_ID)));
                    task.setPriority_level(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_PRIORITY_LEVEL)));
                    task.setIs_done(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_IS_DONE)) == 1 );
                    tasks.add(task);
                }
            }
        }
        // return All tasks
        return tasks;
    }

    /**
     * Updating a task
     * @param task
     * @return
     */
    public int updateTask(Task task) {
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.TASK_COLUMN_LABEL, task.getLabel());
        values.put(DatabaseHelper.TASK_COLUMN_NOTE, task.getNote());
        values.put(DatabaseHelper.TASK_COLUMN_START_DATE, task.getStart_date());
        values.put(DatabaseHelper.TASK_COLUMN_END_DATE, task.getEnd_date());
        values.put(DatabaseHelper.TASK_COLUMN_USER_ID, task.getUser_id());
        values.put(DatabaseHelper.TASK_COLUMN_PRIORITY_LEVEL, task.getPriority_level());
        values.put(DatabaseHelper.TASK_COLUMN_IS_DONE, task.isIs_done());

        // updating row
        return database.update(DatabaseHelper.TASK_TABLE_NAME, values, DatabaseHelper.TASK_COLUMN_ID + "=? ",new String[] { String.valueOf(task.getId())});
    }

    /**
     * Remove a task
     * @param id
     */
    public void removeTask(long id) {
        database.delete(DatabaseHelper.TASK_TABLE_NAME, DatabaseHelper.TASK_COLUMN_ID + "=" + id, null);
    }



}
