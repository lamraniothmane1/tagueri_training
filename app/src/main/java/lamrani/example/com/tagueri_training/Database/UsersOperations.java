package lamrani.example.com.tagueri_training.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import lamrani.example.com.tagueri_training.Models.Task;
import lamrani.example.com.tagueri_training.Models.User;

/**
 * Created by Lamrani on 27/02/2018.
 */

public class UsersOperations {

    public static final String LOGTAG = "USERS_OPERATIONS";
    SQLiteOpenHelper dbHandler;
    SQLiteDatabase database;
    private static final String[] allColumns = {
            DatabaseHelper.USER_COLUMN_ID,
            DatabaseHelper.USER_COLUMN_FULL_NAME,
            DatabaseHelper.USER_COLUMN_EMAIL,
            DatabaseHelper.USER_COLUMN_PASSWORD
    };

    public UsersOperations(Context context){
        dbHandler = new DatabaseHelper(context);
    }

    public void open(){
        Log.i(LOGTAG,"Database Opened");
        database = dbHandler.getWritableDatabase();

    }

    public void close(){
        Log.i(LOGTAG, "Database Closed");
        dbHandler.close();
    }

    /**
     * Add new User
     * @param user
     * @return
     */
    public User addUser(User user){
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.USER_COLUMN_FULL_NAME, user.getFull_name());
        values.put(DatabaseHelper.USER_COLUMN_EMAIL, user.getEmail());
        values.put(DatabaseHelper.USER_COLUMN_PASSWORD, user.getPassword());
        long insertId = database.insert(DatabaseHelper.USER_TABLE_NAME,null,values);
        user.setId(insertId);
        return user;
    }


    /**
     * Get a user by its email and password
     * @param email
     * @param password
     * @return
     */
    public User findUser(String email, String password){
        Cursor cursor = database.query(DatabaseHelper.USER_TABLE_NAME ,allColumns, DatabaseHelper.USER_COLUMN_EMAIL + "=?",new String[]{String.valueOf(email)},null,null, null, null);
        if (cursor != null)
            if(cursor.getCount() > 0){
                cursor.moveToFirst();

                if(cursor.getString(cursor.getColumnIndex(DatabaseHelper.USER_COLUMN_PASSWORD)).equals(password)){
                    User user = new User();
                    user.setId(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_ID)));
                    user.setFull_name(cursor.getString(cursor.getColumnIndex(DatabaseHelper.USER_COLUMN_FULL_NAME)));
                    user.setEmail(cursor.getString(cursor.getColumnIndex(DatabaseHelper.USER_COLUMN_EMAIL)));
                    user.setPassword(cursor.getString(cursor.getColumnIndex(DatabaseHelper.USER_COLUMN_PASSWORD)));

                    return user;
                }

            }
            return null;
    }

    /**
     * Get a user by its ID
     * @param id
     * @return
     */
    public User findUserByID(long id){
        Cursor cursor = database.query(DatabaseHelper.USER_TABLE_NAME ,allColumns, DatabaseHelper.USER_COLUMN_ID + "=?",new String[]{String.valueOf(id)},null,null, null, null);
        if (cursor != null)
            if(cursor.getCount() > 0){
                cursor.moveToFirst();
                User user = new User();
                user.setId(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TASK_COLUMN_ID)));
                user.setFull_name(cursor.getString(cursor.getColumnIndex(DatabaseHelper.USER_COLUMN_FULL_NAME)));
                user.setEmail(cursor.getString(cursor.getColumnIndex(DatabaseHelper.USER_COLUMN_EMAIL)));
                user.setPassword(cursor.getString(cursor.getColumnIndex(DatabaseHelper.USER_COLUMN_PASSWORD)));

                return user;
            }
        return null;
    }


    /**
     * Updating a user
     * @param user
     * @return
     */
    public int updateUser(User user) {
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.USER_COLUMN_FULL_NAME, user.getFull_name());
        values.put(DatabaseHelper.USER_COLUMN_EMAIL, user.getEmail());
        values.put(DatabaseHelper.USER_COLUMN_PASSWORD, user.getPassword());

        // updating row
        return database.update(DatabaseHelper.USER_TABLE_NAME, values, DatabaseHelper.USER_COLUMN_ID + "=? ",new String[] { String.valueOf(user.getId())});
    }

    public boolean checkUser(String email){
        boolean result = false;

        Cursor cursor = database.query(DatabaseHelper.USER_TABLE_NAME ,allColumns, DatabaseHelper.USER_COLUMN_EMAIL + "=?",new String[]{email},null,null, null, null);
        if (cursor != null)
            if(cursor.getCount() > 0){
                result = true;
            }

        return result;
    }

    /**
     * Remove a user
     * @param id
     */
    public void removeUser(long id) {
        database.delete(DatabaseHelper.USER_TABLE_NAME, DatabaseHelper.USER_COLUMN_ID + "=" + id, null);
    }



}
