package lamrani.example.com.tagueri_training.UI;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import lamrani.example.com.tagueri_training.Database.UsersOperations;
import lamrani.example.com.tagueri_training.Models.User;
import lamrani.example.com.tagueri_training.R;
import lamrani.example.com.tagueri_training.Utils.Helper;

public class RegisterActivity extends AppCompatActivity {

    EditText input_fullname, input_email, input_password;
    Button btn_save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        if(getSupportActionBar() != null){
            getSupportActionBar().hide();
        }

        // set the locale language
        Helper.setLocale(RegisterActivity.this);

        configView();

    }

    @Override
    protected void onResume() {
        super.onResume();
        Helper.setLocale(RegisterActivity.this);
    }

    /**
     * Initialize UI components
     */
    private void configView() {
        input_fullname = findViewById(R.id.input_user_full_name);
        input_email = findViewById(R.id.input_email);
        input_password = findViewById(R.id.input_password);

        btn_save = findViewById(R.id.btn_save_user);
        btn_save.setOnClickListener(new SaveUserClickListener());
    }


    private class SaveUserClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {

            if (!validate()) {
                onRegistrationFailed();
                return;
            }

            btn_save.setEnabled(false);

            // On complete call either onLoginSuccess or onLoginFailed
            onRegistrationSuccess();

        }
    }

    private void onRegistrationSuccess() {
        btn_save.setEnabled(true);

        String fullname = input_fullname.getText().toString();
        String email = input_email.getText().toString();
        String password = input_password.getText().toString();

        UsersOperations usersOperations = new UsersOperations(getApplicationContext());
        usersOperations.open();

        if (usersOperations.checkUser(email)){
            Toast.makeText(this, getString(R.string.user_already_exists), Toast.LENGTH_SHORT).show();
        }
        else{
            User user = new User();
            user.setFull_name(fullname);
            user.setEmail(email);
            user.setPassword(password);

            User created = usersOperations.addUser(user);

            usersOperations.close();

            Toast.makeText(RegisterActivity.this, getString(R.string.user_created_successfully), Toast.LENGTH_SHORT).show();

            finish();
        }

    }

    private void onRegistrationFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();

        btn_save.setEnabled(true);
    }

    private boolean validate() {
        boolean valid = true;

        String email = input_email.getText().toString();
        String password = input_password.getText().toString();
        String full_name = input_fullname.getText().toString();

        // checking the name
        if (full_name.isEmpty()) {
            input_fullname.setError(getString(R.string.mail_validation));
            valid = false;
        } else {
            input_fullname.setError(null);
            // checking the email
            if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                input_email.setError(getString(R.string.mail_validation));
                valid = false;
            } else {
                input_email.setError(null);

                // checking the password
                if (password.isEmpty() || password.length() < 4 ) {
                    input_password.setError(getString(R.string.password_validation));
                    valid = false;
                } else {
                    input_password.setError(null);
                }
            }
        }





        return valid;
    }
}
