package lamrani.example.com.tagueri_training.UI.fragments;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lamrani.example.com.tagueri_training.Adapters.AllUsersTasksAdapter;
import lamrani.example.com.tagueri_training.Adapters.UserTasksAdapter;
import lamrani.example.com.tagueri_training.Database.TasksOperations;
import lamrani.example.com.tagueri_training.Database.UsersOperations;
import lamrani.example.com.tagueri_training.Models.Task;
import lamrani.example.com.tagueri_training.Models.User;
import lamrani.example.com.tagueri_training.R;
import lamrani.example.com.tagueri_training.UI.MainActivity;
import lamrani.example.com.tagueri_training.Utils.Helper;

/**
 * Created by Lamrani on 19/09/2018.
 */

public class UserTasksFragment extends MainTasksFragment{

    UserTasksAdapter userTasksAdapter;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Floating add button
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setVisibility(View.VISIBLE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                //hideFragments();
                ((MainActivity)activity).showFragment(new AddTaskFragment());
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
        activity.setTitle(getString(R.string.menu_user_tasks));
        activity.findViewById(R.id.searchView).setVisibility(View.VISIBLE);
    }


    @Override
    public void filterList(String s) {
        // remove all items
        removeAllItems();

        List<Task> tasks_match = new ArrayList<>();
        for(Task task : user_tasks){
            if(task.getLabel().toLowerCase().contains(s.toLowerCase())){
                tasks_match.add(task);
            }

            populateRecyclerView(tasks_match);
        }
    }


    /**
     * Configure recycler view
     */
    @Override
    public void configureRecyclerView() {
        recyclerView = view.findViewById(R.id.recycler_view);
        // To make scrolling smoothly
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        userTasksAdapter = new UserTasksAdapter((MainActivity) getActivity());

        recyclerView.setAdapter(userTasksAdapter);

    }

    @Override
    protected void removeAllItems() {
        userTasksAdapter.removeAllItems();
    }

    @Override
    protected void addItem(Task task) {
        userTasksAdapter.addItem(task);
    }

    /**
     * Refreshing the list
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void refreshListView(int code, Date date) {
        TasksOperations tasksOperations = new TasksOperations(activity);
        tasksOperations.open();

        // get the current user_id
        User user = Helper.getCurrentUser(activity);
        long id_user = user.getId();

        switch (code){
            case 0:{
                user_tasks = tasksOperations.getTasksByDate(id_user, date);
                break;
            }
            case 1:{
                user_tasks = tasksOperations.getTasksByWeek(id_user, date);
                break;
            }
            case 2:{
                user_tasks = tasksOperations.getTasksByMonth(id_user, date);
                break;
            }
            case 3:{
                user_tasks = tasksOperations.getAllUserTasks(id_user);
                break;
            }
        }

        populateRecyclerView(user_tasks);

        tasksOperations.close();
    }
}
