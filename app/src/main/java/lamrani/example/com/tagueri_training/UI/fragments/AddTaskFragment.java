package lamrani.example.com.tagueri_training.UI.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import lamrani.example.com.tagueri_training.Database.TasksOperations;
import lamrani.example.com.tagueri_training.Models.Task;
import lamrani.example.com.tagueri_training.Models.User;
import lamrani.example.com.tagueri_training.R;
import lamrani.example.com.tagueri_training.UI.MainActivity;
import lamrani.example.com.tagueri_training.Utils.Constants;
import lamrani.example.com.tagueri_training.Utils.Helper;

/**
 * Created by Lamrani on 19/09/2018.
 */

public class AddTaskFragment extends TaskFragment {

    @Override
    public void onResume() {
        super.onResume();
        // set the title
        if(activity.getSupportActionBar() != null){
            activity.setTitle(getString(R.string.new_task));
        }
    }


    @Override
    @RequiresApi(api = Build.VERSION_CODES.N)
    protected void initializeValues() {
        // set the current date to textViews
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        tv_start_date.setText(dateFormat.format(date));
        tv_end_date.setText(dateFormat.format(date));
    }

    @Override
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void save(){
        // Getting the full start and end dates from string, then we are going to compare them (start date
        String st_start_date = tv_start_date.getText().toString() + " " + tv_start_time.getText().toString();
        String st_end_date = tv_end_date.getText().toString() + " " + tv_end_time.getText().toString();
        String label = input_label.getText().toString();
        String note = input_note.getText().toString();

        // get selected radio button from radioGroup for priority level
        String priority_level = getSelectedPriorityLevel();

        Date start_date = Helper.getDateTime(st_start_date);
        Date end_date = Helper.getDateTime(st_end_date);

        // verifying that all the fields are not empty
        if(start_date != null && end_date != null && !label.isEmpty()){
            // check if the start date is less than the end date
            if(start_date.after(end_date)){
                Toast.makeText(activity, getString(R.string.verify_the_time), Toast.LENGTH_SHORT).show();
            }
            else{
                Task task = new Task();
                // set the label
                task.setLabel(label);
                // set the note
                task.setNote(note);
                // set the start date
                task.setStart_date(st_start_date);
                // set the end date
                task.setEnd_date(st_end_date);
                // set the priority level
                task.setPriority_level(priority_level);
                // set the task status
                task.setIs_done(switch_status.isChecked());
                // set the user_id
                User user = Helper.getCurrentUser(activity);
                task.setUser_id(user.getId());

                // save it into database
                TasksOperations tasksOperations = new TasksOperations(activity);
                tasksOperations.open();
                tasksOperations.addTask(task);
                tasksOperations.close();

                Toast.makeText(activity, getString(R.string.task_created_successfully), Toast.LENGTH_SHORT).show();
                activity.onBackPressed();
            }
        }
        else{
            Toast.makeText(activity, getString(R.string.verify_the_fields), Toast.LENGTH_SHORT).show();
        }
    }


}
