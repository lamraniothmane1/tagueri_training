package lamrani.example.com.tagueri_training.UI.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.res.ColorStateList;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.SearchView;
import android.widget.TextView;

import com.whiteelephant.monthpicker.MonthPickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import lamrani.example.com.tagueri_training.Adapters.AllUsersTasksAdapter;
import lamrani.example.com.tagueri_training.Adapters.UserTasksAdapter;
import lamrani.example.com.tagueri_training.Database.TasksOperations;
import lamrani.example.com.tagueri_training.Database.UsersOperations;
import lamrani.example.com.tagueri_training.Models.Task;
import lamrani.example.com.tagueri_training.Models.User;
import lamrani.example.com.tagueri_training.R;
import lamrani.example.com.tagueri_training.UI.MainActivity;
import lamrani.example.com.tagueri_training.Utils.Constants;
import lamrani.example.com.tagueri_training.Utils.Helper;

/**
 * Created by Lamrani on 19/09/2018.
 */

public class AllUsersTasksFragment extends MainTasksFragment{

    AllUsersTasksAdapter allUsersTasksAdapter;

    @Override
    public void onResume() {
        super.onResume();
        activity.setTitle(getString(R.string.menu_users_tasks));
        activity.findViewById(R.id.searchView).setVisibility(View.VISIBLE);
    }



    @Override
    public void filterList(String s) {
        // remove all items
        removeAllItems();

        // check if the label or the username match the String
        UsersOperations usersOperations = new UsersOperations(activity);
        List<Task> tasks_match = new ArrayList<>();
        for(Task task : user_tasks){
            usersOperations.open();
            User user = usersOperations.findUserByID(task.getUser_id());

            String user_name = user.getFull_name();

            if(task.getLabel().toLowerCase().contains(s.toLowerCase()) || user_name.toLowerCase().contains(s.toLowerCase())){
                tasks_match.add(task);
            }

            populateRecyclerView(tasks_match);
        }
    }


    /**
     * Configure recycler view
     */
    @Override
    public void configureRecyclerView() {
        recyclerView = view.findViewById(R.id.recycler_view);
        // To make scrolling smoothly
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        allUsersTasksAdapter = new AllUsersTasksAdapter((MainActivity) getActivity());

        recyclerView.setAdapter(allUsersTasksAdapter);

    }

    @Override
    protected void removeAllItems() {
        allUsersTasksAdapter.removeAllItems();
    }

    @Override
    protected void addItem(Task task) {
        allUsersTasksAdapter.addItem(task);
    }

    /**
     * Refreshing the list
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void refreshListView(int code, Date date) {
        TasksOperations tasksOperations = new TasksOperations(activity);
        tasksOperations.open();

        user_tasks = new ArrayList<>();

        switch (code){
            case 0:{
                user_tasks = tasksOperations.getTasksByDate(-1, date);
                break;
            }
            case 1:{
                user_tasks = tasksOperations.getTasksByWeek(-1, date);
                break;
            }
            case 2:{
                user_tasks = tasksOperations.getTasksByMonth(-1, date);
                break;
            }
            case 3:{
                User user = Helper.getCurrentUser(activity);
                long id_user = user.getId();
                user_tasks = tasksOperations.getAllTasks();
                break;
            }
        }

        populateRecyclerView(user_tasks);

        tasksOperations.close();
    }
}
