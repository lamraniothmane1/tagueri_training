package lamrani.example.com.tagueri_training.Adapters;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lamrani.example.com.tagueri_training.Database.UsersOperations;
import lamrani.example.com.tagueri_training.Models.Task;
import lamrani.example.com.tagueri_training.Models.User;
import lamrani.example.com.tagueri_training.R;
import lamrani.example.com.tagueri_training.UI.MainActivity;
import lamrani.example.com.tagueri_training.UI.fragments.EditTaskFragment;
import lamrani.example.com.tagueri_training.UI.fragments.ShowTaskFragment;
import lamrani.example.com.tagueri_training.Utils.Constants;
import lamrani.example.com.tagueri_training.Utils.Helper;

/**
 * Created by Lamrani on 19/09/2018.
 */

public class AllUsersTasksAdapter extends RecyclerView.Adapter<AllUsersTasksAdapter.Holder> {

    private List<Task> items_data;
    private MainActivity activity;

    public AllUsersTasksAdapter(MainActivity activity) {
        items_data = new ArrayList<>();
        this.activity = activity;
    }

    @NonNull
    @Override
    public AllUsersTasksAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_task_2, parent, false);

        return new Holder(row);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onBindViewHolder(@NonNull AllUsersTasksAdapter.Holder holder, int position) {
        Task task = getItem(position);
        String label = task.getLabel();
        String date = task.getStart_date(); //+ " - " + task.getEnd_date();

        // get the user
        UsersOperations usersOperations = new UsersOperations(activity);
        usersOperations.open();
        User user = usersOperations.findUserByID(task.getUser_id());
        String user_name = user.getFull_name();

        // priority
        String priority_level = task.getPriority_level();

        if(priority_level.equals(Constants.PRIORITY_HIGH)){
            holder.layout.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(activity, R.color.high)));
        }
        else if(priority_level.equals(Constants.PRIORITY_MEDIUM)){
            holder.layout.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(activity, R.color.medium)));
        }
        else if(priority_level.equals(Constants.PRIORITY_LOW)){

        }

        // check if the task is done or not
        if(task.isIs_done()){
            holder.layout.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(activity, R.color.green_light)));
        }
        else {
            // check if the end date has expired or not yet
            Date end_date = Helper.getDate(task.getEnd_date());
            Date now = new Date();
            if(end_date.before(now) && !task.isIs_done()){
                holder.iv_warning.setVisibility(View.VISIBLE);
            }
            else{
                holder.iv_warning.setVisibility(View.GONE);
            }
        }

        holder.tv_item_label.setText(label);
        holder.tv_item_date.setText(date);
        holder.tv_item_name.setText(user_name);
    }

    @Override
    public int getItemCount() {
        return items_data.size();
    }

    /**
     * Add an item to the list
     * @param task
     */
    public void addItem(Task task){
        items_data.add(task);
        notifyDataSetChanged();
    }

    /**
     * Clear the list
     */
    public void removeAllItems(){
        items_data.clear();
        notifyDataSetChanged();
    }

    /**
     * get an item at position
     */
    public Task getItem(int position){
        return items_data.get(position);
    }


    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private Context context;
        private TextView tv_item_label, tv_item_date, tv_item_name;
        private LinearLayout layout;
        private ImageView iv_warning;

        Holder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            context = itemView.getContext();

            layout = itemView.findViewById(R.id.holder_background);
            tv_item_label = itemView.findViewById(R.id.tv_item_label);
            tv_item_date = itemView.findViewById(R.id.tv_item_date);
            tv_item_name = itemView.findViewById(R.id.tv_item_name);
            iv_warning = itemView.findViewById(R.id.iv_warning);

        }

        @Override
        public void onClick(View view) {

            Task selected_item = getItem(getAdapterPosition());

            ShowTaskFragment showTaskFragment = new ShowTaskFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.TASK, selected_item);
            showTaskFragment.setArguments(bundle);
            activity.showFragment(showTaskFragment);
        }
    }

}
